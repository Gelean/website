## URL

https://gelean.gitlab.io/website

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

## Links

* https://about.gitlab.com/gitlab-ci/
* https://gitlab.com/pages/plain-html/blob/master/public/index.html
* https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
* https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
